infection_anomaly_category_1 = {
    level = 2
    max_once_global = yes

    spawn_chance = {
        modifier = {
            add = 3
            is_planet_class = pc_toxic
            FROM = {
                owner = {
                    has_authority = auth_machine_intelligence
                }
            }
        }
    }

    on_success = {
        1 = infection.101
    }
}
